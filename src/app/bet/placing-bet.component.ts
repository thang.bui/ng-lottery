import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../services/game.service';
import { Bet } from '../services/models.interface';

@Component({
    selector: 'placing-bet',
    styleUrls: ['./placing-bet.component.scss'],
    templateUrl: './placing-bet.component.html'
})
export class PlacingBetComponent {
    bet: Bet;

    constructor(private router: Router, private gameService: GameService) {
        this.bet = {
            player: '',
            betSequences: ['', '', '', '', '', '']
        };
    }

    cancel() {
        this.router.navigate(['/bet']);
    }

    placeBet() {
        this.bet.betSequences.forEach(item => {
                item = item.toString().padStart(2, '0');
        });
        if (this.gameService.bets.findIndex(item => item.player === this.bet.player) !== -1) {
            alert('duplication player');
            return;
        }

        this.gameService.placeBet(this.bet);
        this.router.navigate(['/bet']);
    }
}