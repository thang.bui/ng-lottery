import { Component } from '@angular/core';

@Component({
    selector: 'app-header',
    styleUrls: ['./header.component.scss'],
    template: `
        <div class="wrapper">
            <h1>Welcome to Angular Lottery</h1>
            <h5>Let try your luck</h5>
            <ul>
                <li>
                    <a routerLink="/home" [routerLinkActive]="['activated']">Home</a>
                    <span></span>
                </li>
                <li>
                    <a routerLink="/bet" [routerLinkActive]="['activated']">Bet</a>
                    <span></span>
                </li>
                <li>
                    <a routerLink="/about">About</a>
                    <span></span>
                </li>
            </ul>
        </div>    
    `
})
export class HeaderComponent {}