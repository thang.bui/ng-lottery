import { Injectable } from '@angular/core';
import { HistoryItem } from './models.interface';

@Injectable()
export class HistoryService {
    private _histories: HistoryItem[];
    constructor() {
        this.initDefaultHistories();
    }

    getHistories(): HistoryItem[] {
        return [...this._histories];
    }

    add(player: string, sequence: string[]) {
        this._histories = [
            { player: player, betSequences: sequence, date: new Date().toDateString() },
            ...this._histories
        ];
    }

    private initDefaultHistories() {
        this._histories = [
            { player: 'Jon Doe', date: '2017-10-26',
                betSequences: ['01', '02', '03', '04', '05', '06'] } as HistoryItem,
            
            { player: 'Thang Bui', date: '2017-09-26',
                betSequences: ['11', '12', '13', '14', '15', '16'] } as HistoryItem,
        ];
    }

}
